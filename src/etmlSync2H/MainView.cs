﻿using Shell32;
using System;
using System.Diagnostics;
using System.IO;
using System.Security.Principal;
using System.Windows.Forms;
//TODO
//1) LOG des transferts effectués
//2) Regarder bug corrigé pour récursion ? (structure sous-dossiers)
//3) Possibilité de définir un sous-dossier depuis la racine du dossier élève (création si pas trouvé)
//4) 
namespace draganddropSAM
{
    public partial class DropHere : Form
    {
        enum Mode { CHECK, COPY, DELETE };

        static bool overwrite = false;//TODO GUI

        string TRASH_FOLDER = @".\etmlSync2H-Trash";
        const string LOG_FILENAME = "etmSync2H-Log";

        /*
         * Trials to move to standard trash... but as network file it's not working
         * 
        public static Shell shell = new Shell();
        public static Folder RecyclingBin = shell.NameSpace(10);
      

        SecurityIdentifier user = WindowsIdentity.GetCurrent().User;

        */


        string[] filePaths;

        public static object UserPrincipal { get; private set; }

        public DropHere()
        {
            InitializeComponent();
            if (!File.Exists(TRASH_FOLDER))
                Directory.CreateDirectory(TRASH_FOLDER);

        }

        private void Form1_Load(object sender, EventArgs e)
        {

            //autorise the drag and drop in a form
            this.AllowDrop = true;
            this.DragEnter += Form1_DragEnter;
            //this.DragDrop += Form1_DragDrop;


        }

        private void Form1_DragDrop(object sender, DragEventArgs e)
        {
            zoneDepotLbl.Visible = false;

            //path of the heleves, may change
            //string helevespath = hPathTxt.Text;
            //string helevespath = @"C: \Users\malherbesa\Desktop\heleves";

            if (hPathTxt.Text == "" || !Directory.Exists(hPathTxt.Text))
            {
                MessageBox.Show("Erreur de chemin vers le H");
                return;
            }

            //IF a file a is dropped in the form do the following
            if (e.Data.GetDataPresent(DataFormats.FileDrop))
            {

                //allow multiple file drop
                filePaths = (string[])(e.Data.GetData(DataFormats.FileDrop));
                WalkDirectories(Mode.CHECK);

                /*
                btnDelete.Enabled = true;
                btnTransfer.Enabled = true;
                */
            }
        }

        private void WalkDirectories(Mode mode)
        {
            foreach (string fileLoc in filePaths)
            {
                //call "ProcessDirectory" with the path of the H and the filepath of the dropped file
                SearchMatching(hPathTxt.Text, fileLoc, txtTargetSubFolder.Text, mode);
            }
        }

        private void Form1_DragEnter(object sender, DragEventArgs e)
        {
            if (e.Data.GetDataPresent(DataFormats.FileDrop))
            {
                e.Effect = DragDropEffects.Copy;
            }
            else
            {
                e.Effect = DragDropEffects.None;
            }
        }

        private void SearchMatching(string root, string studentFilePath, string targetSubFolder, Mode mode)
        {
            string studentFilenameWithExtension = Path.GetFileName(studentFilePath);
            string studentFilenameWithoutExtension = Path.GetFileNameWithoutExtension(studentFilePath);

            string studentLogin = studentFilenameWithoutExtension.Split(new String[] { cmbFileNameSeparator.Text }, StringSplitOptions.None)[Convert.ToInt32(cmbLoginPositionInFileName.Text)];
            string prefix = txtPrefix.Text;

            //Répertoire CINA,CINB,MIN,...
            foreach (string type in Directory.GetDirectories(root))
            {
                //1,2,3
                foreach (string year in Directory.GetDirectories(type))
                {
                    //malherbesa...
                    foreach (string studentLoginPath in Directory.GetDirectories(year))
                    {
                        //Check matching
                        string folderName = Path.GetFileName(Path.GetDirectoryName(studentLoginPath + @"\"));
                        string targetPath;


                        //Match found
                        if (studentLogin.Equals(folderName))
                        {
                            if (targetSubFolder != "")
                            {
                                targetSubFolder += @"\";

                                string subFolderPath = studentLoginPath + "\\" + targetSubFolder;
                                if (!File.Exists(subFolderPath))
                                {
                                    switch (mode)
                                    {
                                        case Mode.CHECK:
                                            if (chkCreateSubfolder.Checked)
                                                Log("+MKDIR " + subFolderPath, mode, false);
                                            else
                                                Log("-Subdirectory missing " + subFolderPath + "Check 'folder creation' option", mode, false);
                                            break;
                                        case Mode.COPY:
                                            if(chkCreateSubfolder.Checked)
                                                Directory.CreateDirectory(subFolderPath);
                                            break;
                                    }

                                }
                            }
                            if (prefix != "")
                            {
                                prefix += @"\";
                            }


                            targetPath = studentLoginPath + "\\" + targetSubFolder + txtPrefix.Text + studentFilenameWithExtension;

                            //Copy file to the H 
                            string trashPath = Path.GetFullPath(TRASH_FOLDER);
                            try
                            {

                                if (!File.Exists(targetPath))
                                {
                                    switch (mode)
                                    {
                                        case Mode.CHECK:
                                            Log("Ready to copy " + studentFilenameWithExtension + " -> " + targetPath, mode, false);
                                            break;

                                        case Mode.COPY:
                                            File.Copy(studentFilePath, targetPath, false);
                                            Log("" + studentFilenameWithExtension + " copied -> " + targetPath, mode, false);
                                            break;

                                        case Mode.DELETE:
                                            Log(targetPath + " does not exist", mode, true);
                                            break;
                                    }

                                }
                                //File already exists
                                else
                                {
                                    overwrite = overwriteCheck.Checked;
                                    FileInfo existing = new FileInfo(targetPath);
                                    FileInfo newFile = new FileInfo(studentFilePath);

                                    string existingInfo = "[date: " + existing.LastWriteTime + " | size:" + existing.Length / 1024 + "ko]";
                                    string newFileInfo = "[date: " + newFile.LastWriteTime + " | size:" + newFile.Length / 1024 + "ko]";

                                    switch (mode)
                                    {
                                        case Mode.CHECK:
                                            Log(targetPath + existingInfo + " exists. It will " + (overwrite ? "" : "not") + " be overwritten with " + studentFilePath + newFileInfo + (overwrite ? "" : " (Unless overwrite checked)"), mode, false);

                                            break;

                                        case Mode.COPY:
                                            if (overwrite)
                                            {
                                                File.Copy(studentFilePath, targetPath, overwrite);
                                                Log(targetPath + existingInfo + " overwritten with new file " + newFileInfo, mode, false);
                                            }
                                            else
                                            {
                                                Log(targetPath + existingInfo + " exists and overwrite disabled (no copy)", mode, true);
                                            }
                                            break;

                                        case Mode.DELETE:
                                            //File.Delete(targetPath);
                                            //RecyclingBin.CopyHere(targetPath);
                                            if (chkTrash.Enabled)
                                            {
                                                trashPath += @"\" + studentFilenameWithExtension;

                                                const string COPY_PREFIX = " (Version ";
                                                int i = 0;
                                                while (File.Exists(trashPath))
                                                {
                                                    string fileWithoutExtension = Path.GetFileNameWithoutExtension(trashPath);

                                                    //Replace copie ii...
                                                    if (i == 0)
                                                    {
                                                        fileWithoutExtension += COPY_PREFIX + i + ")";
                                                    }
                                                    else
                                                        fileWithoutExtension = fileWithoutExtension.Replace(COPY_PREFIX + (i-1) + ")", COPY_PREFIX + i + ")");

                                                    trashPath = Path.GetFullPath(TRASH_FOLDER) + @"\" + fileWithoutExtension + Path.GetExtension(trashPath);
                                                    i++;
                                                }

                                                File.Move(targetPath, trashPath);
                                                Log(targetPath + " moved to " + trashPath, mode, false);
                                            }
                                            else
                                            {
                                                File.Delete(targetPath);
                                                Log(targetPath + " deleted", mode, false);
                                            }


                                            break;
                                    }

                                }

                            }
                            catch (Exception e)
                            {
                                switch (mode)
                                {
                                    case Mode.CHECK:
                                        break;
                                    case Mode.COPY:
                                        Log("Impossible de copier " + studentFilenameWithExtension + " dans " + targetPath + "(" + e.Message + ")", mode, true);
                                        break;
                                    case Mode.DELETE:
                                        Log("Impossible de supprimer " + targetPath + " (Poubelle = " + trashPath + ")[" + e.Message + "]", mode, true);
                                        break;
                                }


                            }

                            //TROUVÉ
                            return;
                        }
                    }
                }
            }
        }

        private void Log(string message, Mode mode, bool error, bool append = false)
        {

            TextBox target = null;

            switch (mode)
            {
                case Mode.CHECK:
                    target = txtPreview;
                    break;

                case Mode.COPY:
                case Mode.DELETE:
                    target = error ? txtKo : txtOk;
                    break;

            }

            string finalMessage = message + (append ? "" : Environment.NewLine);
            target.Text += finalMessage;


            File.AppendAllText(LOG_FILENAME + (error ? ".err" : "") + ".txt", DateTime.Now + " | " + finalMessage);
        }

        private void overwriteCheck_CheckedChanged(object sender, EventArgs e)
        {
            /*
            btnDelete.Enabled = false;
            btnTransfer.Enabled = false;
            */
        }

        private void btnTransfer_Click(object sender, EventArgs e)
        {
            WalkDirectories(Mode.COPY);
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            WalkDirectories(Mode.DELETE);
        }

        private void btnTrash_Click(object sender, EventArgs e)
        {

            openFile(TRASH_FOLDER, true);
        }

        private void btnRefresh_Click(object sender, EventArgs e)
        {
            txtKo.Clear();
            txtOk.Clear();
            txtPreview.Clear();

            WalkDirectories(Mode.CHECK);

        }

        private void logToolStripMenuItem_Click(object sender, EventArgs e)
        {
            showLog(false);
        }

        private void showLog(bool errorLog)
        {
            string logFile = LOG_FILENAME + (errorLog ? ".err" : "") + ".txt";

            openFile(logFile);
        }

        private void errorLogToolStripMenuItem_Click(object sender, EventArgs e)
        {
            showLog(true);
        }

        private void corbeilleToolStripMenuItem_Click(object sender, EventArgs e)
        {
            openFile(TRASH_FOLDER, true);
        }

        private void openFile(string relativePath, bool isDirectory = false)
        {
            string fullPath = Path.GetFullPath(relativePath);

            if (isDirectory)
            {
                if (!Directory.Exists(fullPath))
                {
                    Directory.CreateDirectory(fullPath);
                }
            }
            else
            {
                if (!File.Exists(fullPath))
                {
                    using (FileStream fs = File.Create(fullPath))
                    {
                        //clean ressource
                    }
                }
            }


            Process.Start(new ProcessStartInfo(fullPath));
        }

        private void quitterToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
