﻿namespace draganddropSAM
{
    partial class DropHere
    {
        /// <summary>
        /// Variable nécessaire au concepteur.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Nettoyage des ressources utilisées.
        /// </summary>
        /// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Code généré par le Concepteur Windows Form

        /// <summary>
        /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas
        /// le contenu de cette méthode avec l'éditeur de code.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DropHere));
            this.overwriteCheck = new System.Windows.Forms.CheckBox();
            this.txtTargetSubFolder = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.cmbFileNameSeparator = new System.Windows.Forms.ComboBox();
            this.zoneDepotLbl = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.hPathTxt = new System.Windows.Forms.TextBox();
            this.txtPreview = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.cmbLoginPositionInFileName = new System.Windows.Forms.ComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.txtPrefix = new System.Windows.Forms.TextBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.chkCreateSubfolder = new System.Windows.Forms.CheckBox();
            this.txtOk = new System.Windows.Forms.TextBox();
            this.txtKo = new System.Windows.Forms.TextBox();
            this.btnTransfer = new System.Windows.Forms.Button();
            this.btnDelete = new System.Windows.Forms.Button();
            this.chkTrash = new System.Windows.Forms.CheckBox();
            this.btnRefresh = new System.Windows.Forms.Button();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fichierToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.quitterToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.outilsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.logToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.errorLogToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.corbeilleToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.groupBox1.SuspendLayout();
            this.menuStrip1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.SuspendLayout();
            // 
            // overwriteCheck
            // 
            this.overwriteCheck.AutoSize = true;
            this.overwriteCheck.Location = new System.Drawing.Point(13, 29);
            this.overwriteCheck.Name = "overwriteCheck";
            this.overwriteCheck.Size = new System.Drawing.Size(209, 17);
            this.overwriteCheck.TabIndex = 1;
            this.overwriteCheck.Text = "Écraser la destination si elle existe déjà";
            this.overwriteCheck.UseVisualStyleBackColor = true;
            this.overwriteCheck.CheckedChanged += new System.EventHandler(this.overwriteCheck_CheckedChanged);
            // 
            // txtTargetSubFolder
            // 
            this.txtTargetSubFolder.Location = new System.Drawing.Point(504, 76);
            this.txtTargetSubFolder.Name = "txtTargetSubFolder";
            this.txtTargetSubFolder.Size = new System.Drawing.Size(197, 20);
            this.txtTargetSubFolder.TabIndex = 2;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(364, 79);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(118, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "Sous-dossier dans le H:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(1, 75);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(120, 13);
            this.label2.TabIndex = 5;
            this.label2.Text = "Caractère de séparation";
            // 
            // cmbFileNameSeparator
            // 
            this.cmbFileNameSeparator.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.cmbFileNameSeparator.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbFileNameSeparator.FormattingEnabled = true;
            this.cmbFileNameSeparator.Items.AddRange(new object[] {
            "-",
            "_"});
            this.cmbFileNameSeparator.Location = new System.Drawing.Point(250, 72);
            this.cmbFileNameSeparator.Name = "cmbFileNameSeparator";
            this.cmbFileNameSeparator.Size = new System.Drawing.Size(71, 24);
            this.cmbFileNameSeparator.TabIndex = 6;
            this.cmbFileNameSeparator.Text = "-";
            // 
            // zoneDepotLbl
            // 
            this.zoneDepotLbl.AutoSize = true;
            this.zoneDepotLbl.BackColor = System.Drawing.Color.White;
            this.zoneDepotLbl.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.zoneDepotLbl.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.zoneDepotLbl.Location = new System.Drawing.Point(221, 247);
            this.zoneDepotLbl.Name = "zoneDepotLbl";
            this.zoneDepotLbl.Size = new System.Drawing.Size(538, 37);
            this.zoneDepotLbl.TabIndex = 7;
            this.zoneDepotLbl.Text = "ZONE DE DÉPÔT POUR ANALYSE";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(1, 21);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(128, 13);
            this.label3.TabIndex = 9;
            this.label3.Text = "Dossier racine pour les H:";
            // 
            // hPathTxt
            // 
            this.hPathTxt.Location = new System.Drawing.Point(134, 18);
            this.hPathTxt.Name = "hPathTxt";
            this.hPathTxt.Size = new System.Drawing.Size(187, 20);
            this.hPathTxt.TabIndex = 8;
            this.hPathTxt.Text = "\\\\setmseb2k120002\\INF_eleves$";
            // 
            // txtPreview
            // 
            this.txtPreview.Location = new System.Drawing.Point(6, 192);
            this.txtPreview.Multiline = true;
            this.txtPreview.Name = "txtPreview";
            this.txtPreview.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.txtPreview.Size = new System.Drawing.Size(982, 168);
            this.txtPreview.TabIndex = 11;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(12, 26);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(583, 52);
            this.label4.TabIndex = 12;
            this.label4.Text = resources.GetString("label4.Text");
            // 
            // cmbLoginPositionInFileName
            // 
            this.cmbLoginPositionInFileName.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.cmbLoginPositionInFileName.FormattingEnabled = true;
            this.cmbLoginPositionInFileName.Items.AddRange(new object[] {
            "0",
            "1",
            "2",
            "3",
            "4",
            "5",
            "6",
            "7"});
            this.cmbLoginPositionInFileName.Location = new System.Drawing.Point(250, 47);
            this.cmbLoginPositionInFileName.Name = "cmbLoginPositionInFileName";
            this.cmbLoginPositionInFileName.Size = new System.Drawing.Size(71, 21);
            this.cmbLoginPositionInFileName.TabIndex = 14;
            this.cmbLoginPositionInFileName.Text = "1";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(1, 50);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(244, 13);
            this.label5.TabIndex = 13;
            this.label5.Text = "Position du login dans le nom de fichier (X0-Y1-Z2)";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(364, 50);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(132, 13);
            this.label6.TabIndex = 16;
            this.label6.Text = "Préfixe à ajouter au fichier:";
            // 
            // txtPrefix
            // 
            this.txtPrefix.Location = new System.Drawing.Point(504, 47);
            this.txtPrefix.Name = "txtPrefix";
            this.txtPrefix.Size = new System.Drawing.Size(197, 20);
            this.txtPrefix.TabIndex = 15;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.chkCreateSubfolder);
            this.groupBox1.Controls.Add(this.overwriteCheck);
            this.groupBox1.Location = new System.Drawing.Point(749, 79);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(239, 107);
            this.groupBox1.TabIndex = 17;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Options d\'écriture";
            // 
            // chkCreateSubfolder
            // 
            this.chkCreateSubfolder.AutoSize = true;
            this.chkCreateSubfolder.Location = new System.Drawing.Point(13, 51);
            this.chkCreateSubfolder.Name = "chkCreateSubfolder";
            this.chkCreateSubfolder.Size = new System.Drawing.Size(167, 17);
            this.chkCreateSubfolder.TabIndex = 2;
            this.chkCreateSubfolder.Text = "Créer le sous-dossier si besoin";
            this.chkCreateSubfolder.UseVisualStyleBackColor = true;
            // 
            // txtOk
            // 
            this.txtOk.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtOk.ForeColor = System.Drawing.Color.Green;
            this.txtOk.Location = new System.Drawing.Point(6, 446);
            this.txtOk.Multiline = true;
            this.txtOk.Name = "txtOk";
            this.txtOk.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.txtOk.Size = new System.Drawing.Size(982, 117);
            this.txtOk.TabIndex = 18;
            // 
            // txtKo
            // 
            this.txtKo.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtKo.ForeColor = System.Drawing.Color.Red;
            this.txtKo.Location = new System.Drawing.Point(6, 581);
            this.txtKo.Multiline = true;
            this.txtKo.Name = "txtKo";
            this.txtKo.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.txtKo.Size = new System.Drawing.Size(982, 90);
            this.txtKo.TabIndex = 19;
            // 
            // btnTransfer
            // 
            this.btnTransfer.Location = new System.Drawing.Point(118, 22);
            this.btnTransfer.Name = "btnTransfer";
            this.btnTransfer.Size = new System.Drawing.Size(112, 35);
            this.btnTransfer.TabIndex = 20;
            this.btnTransfer.Text = "Transférer";
            this.btnTransfer.UseVisualStyleBackColor = true;
            this.btnTransfer.Click += new System.EventHandler(this.btnTransfer_Click);
            // 
            // btnDelete
            // 
            this.btnDelete.Location = new System.Drawing.Point(236, 22);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(108, 35);
            this.btnDelete.TabIndex = 21;
            this.btnDelete.Text = "Supprimer";
            this.btnDelete.UseVisualStyleBackColor = true;
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // chkTrash
            // 
            this.chkTrash.AutoSize = true;
            this.chkTrash.Checked = true;
            this.chkTrash.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkTrash.Location = new System.Drawing.Point(350, 33);
            this.chkTrash.Name = "chkTrash";
            this.chkTrash.Size = new System.Drawing.Size(115, 17);
            this.chkTrash.TabIndex = 22;
            this.chkTrash.Text = "Avec backup local";
            this.chkTrash.UseVisualStyleBackColor = true;
            // 
            // btnRefresh
            // 
            this.btnRefresh.Location = new System.Drawing.Point(9, 23);
            this.btnRefresh.Name = "btnRefresh";
            this.btnRefresh.Size = new System.Drawing.Size(103, 35);
            this.btnRefresh.TabIndex = 24;
            this.btnRefresh.Text = "Actualiser";
            this.btnRefresh.UseVisualStyleBackColor = true;
            this.btnRefresh.Click += new System.EventHandler(this.btnRefresh_Click);
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fichierToolStripMenuItem,
            this.outilsToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(1008, 24);
            this.menuStrip1.TabIndex = 25;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fichierToolStripMenuItem
            // 
            this.fichierToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.quitterToolStripMenuItem});
            this.fichierToolStripMenuItem.Name = "fichierToolStripMenuItem";
            this.fichierToolStripMenuItem.Size = new System.Drawing.Size(54, 20);
            this.fichierToolStripMenuItem.Text = "Fichier";
            // 
            // quitterToolStripMenuItem
            // 
            this.quitterToolStripMenuItem.Name = "quitterToolStripMenuItem";
            this.quitterToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.quitterToolStripMenuItem.Text = "Quitter";
            this.quitterToolStripMenuItem.Click += new System.EventHandler(this.quitterToolStripMenuItem_Click);
            // 
            // outilsToolStripMenuItem
            // 
            this.outilsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.corbeilleToolStripMenuItem,
            this.toolStripSeparator1,
            this.logToolStripMenuItem,
            this.errorLogToolStripMenuItem});
            this.outilsToolStripMenuItem.Name = "outilsToolStripMenuItem";
            this.outilsToolStripMenuItem.Size = new System.Drawing.Size(50, 20);
            this.outilsToolStripMenuItem.Text = "Outils";
            // 
            // logToolStripMenuItem
            // 
            this.logToolStripMenuItem.Name = "logToolStripMenuItem";
            this.logToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.logToolStripMenuItem.Text = "Log";
            this.logToolStripMenuItem.Click += new System.EventHandler(this.logToolStripMenuItem_Click);
            // 
            // errorLogToolStripMenuItem
            // 
            this.errorLogToolStripMenuItem.Name = "errorLogToolStripMenuItem";
            this.errorLogToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.errorLogToolStripMenuItem.Text = "Error log";
            this.errorLogToolStripMenuItem.Click += new System.EventHandler(this.errorLogToolStripMenuItem_Click);
            // 
            // corbeilleToolStripMenuItem
            // 
            this.corbeilleToolStripMenuItem.Name = "corbeilleToolStripMenuItem";
            this.corbeilleToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.corbeilleToolStripMenuItem.Text = "Backup";
            this.corbeilleToolStripMenuItem.Click += new System.EventHandler(this.corbeilleToolStripMenuItem_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.btnRefresh);
            this.groupBox2.Controls.Add(this.chkTrash);
            this.groupBox2.Controls.Add(this.btnDelete);
            this.groupBox2.Controls.Add(this.btnTransfer);
            this.groupBox2.Location = new System.Drawing.Point(6, 366);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(982, 75);
            this.groupBox2.TabIndex = 26;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Action";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.label6);
            this.groupBox3.Controls.Add(this.txtPrefix);
            this.groupBox3.Controls.Add(this.cmbLoginPositionInFileName);
            this.groupBox3.Controls.Add(this.label5);
            this.groupBox3.Controls.Add(this.label3);
            this.groupBox3.Controls.Add(this.hPathTxt);
            this.groupBox3.Controls.Add(this.cmbFileNameSeparator);
            this.groupBox3.Controls.Add(this.label2);
            this.groupBox3.Controls.Add(this.label1);
            this.groupBox3.Controls.Add(this.txtTargetSubFolder);
            this.groupBox3.Location = new System.Drawing.Point(12, 79);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(727, 107);
            this.groupBox3.TabIndex = 27;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Options de fichier";
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(149, 6);
            // 
            // DropHere
            // 
            this.AllowDrop = true;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.ClientSize = new System.Drawing.Size(1008, 683);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.txtKo);
            this.Controls.Add(this.txtOk);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.zoneDepotLbl);
            this.Controls.Add(this.txtPreview);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.MinimumSize = new System.Drawing.Size(600, 38);
            this.Name = "DropHere";
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Show;
            this.Text = "Déposez ici les fichiers à transférer dans le H";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.DragDrop += new System.Windows.Forms.DragEventHandler(this.Form1_DragDrop);
            this.DragEnter += new System.Windows.Forms.DragEventHandler(this.Form1_DragEnter);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.CheckBox overwriteCheck;
        private System.Windows.Forms.TextBox txtTargetSubFolder;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox cmbFileNameSeparator;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label zoneDepotLbl;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox hPathTxt;
        private System.Windows.Forms.TextBox txtPreview;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox cmbLoginPositionInFileName;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtPrefix;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.CheckBox chkCreateSubfolder;
        private System.Windows.Forms.TextBox txtOk;
        private System.Windows.Forms.TextBox txtKo;
        private System.Windows.Forms.Button btnTransfer;
        private System.Windows.Forms.Button btnDelete;
        private System.Windows.Forms.CheckBox chkTrash;
        private System.Windows.Forms.Button btnRefresh;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fichierToolStripMenuItem;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.ToolStripMenuItem outilsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem logToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem errorLogToolStripMenuItem;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.ToolStripMenuItem corbeilleToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem quitterToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
    }
}

